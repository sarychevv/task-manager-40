package ru.t1.sarychevv.tm.repository;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.sarychevv.tm.api.repository.IProjectRepository;
import ru.t1.sarychevv.tm.api.repository.ISessionRepository;
import ru.t1.sarychevv.tm.api.repository.ITaskRepository;
import ru.t1.sarychevv.tm.api.service.IConnectionService;
import ru.t1.sarychevv.tm.api.service.IPropertyService;
import ru.t1.sarychevv.tm.comparator.CreatedComparator;
import ru.t1.sarychevv.tm.enumerated.TaskSort;
import ru.t1.sarychevv.tm.marker.DBCategory;
import ru.t1.sarychevv.tm.model.Project;
import ru.t1.sarychevv.tm.model.Task;
import ru.t1.sarychevv.tm.service.ConnectionService;
import ru.t1.sarychevv.tm.service.PropertyService;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Category(DBCategory.class)
public class TaskRepositoryTest {

    private static final Integer NUMBER_OF_ENTRIES = 10;

    private static final String USER_ID_1 = UUID.randomUUID().toString();

    private static final String USER_ID_2 = UUID.randomUUID().toString();

    private static final String TEST_NAME = "Test task";

    private static final String TEST_DESCRIPTION = "Test description";
    @NotNull
    private final IPropertyService propertyService = new PropertyService();
    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);
    @NotNull
    private final SqlSession sqlSession = connectionService.getSqlSession();
    @NotNull
    private List<Task> taskList;
    @NotNull
    private ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
    @NotNull
    private IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);

    @Before
    public void initRepository() throws Exception {
        taskList = new ArrayList<>();
        @NotNull Project project = new Project();
        projectRepository.add(project);
        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            @NotNull Task task = new Task();
            task.setName(TEST_NAME + i);
            task.setDescription(TEST_DESCRIPTION + i);
            if (i <= 5) {
                task.setUserId(USER_ID_1);
                task.setProjectId(project.getId());
                taskRepository.add(USER_ID_1, task);

            } else {
                task.setUserId(USER_ID_2);
                taskRepository.add(USER_ID_2, task);
            }
            taskList.add(task);
        }
    }

    @After
    public void removeRepository() throws Exception {
        taskRepository.removeAll(USER_ID_1);
        taskRepository.removeAll(USER_ID_2);
    }

    @Test
    public void findAllByProjectId() throws Exception {
        Assert.assertNotNull(taskRepository.findAllByProjectId(USER_ID_1, projectRepository.findOneByIndex(USER_ID_1,0).getId()));
    }

    @Test
    public void testAdd() throws Exception {
        Integer expectedNumberOfEntries = NUMBER_OF_ENTRIES + 1;
        @NotNull final String userId = UUID.randomUUID().toString();
        @NotNull Task task = new Task();
        task.setName(TEST_NAME);
        task.setDescription(TEST_DESCRIPTION);
        taskRepository.add(userId, task);
        Assert.assertEquals(expectedNumberOfEntries, Optional.of(taskRepository.getSize(USER_ID_1)));
        @Nullable final Task actualTack = taskRepository.findOneById(userId, task.getId());
        Assert.assertNotNull(actualTack);
        Assert.assertEquals(TEST_NAME, actualTack.getName());
        Assert.assertEquals(TEST_DESCRIPTION, actualTack.getDescription());
        Assert.assertEquals(userId, actualTack.getUserId());
    }

    @Test
    public void testAddTask() throws Exception {
        Integer expectedNumberOfEntries = NUMBER_OF_ENTRIES + 1;
        @NotNull final Task newTask = new Task();
        newTask.setName(TEST_NAME + expectedNumberOfEntries);
        newTask.setDescription(TEST_DESCRIPTION + expectedNumberOfEntries);
        taskRepository.add(USER_ID_1, newTask);
        Assert.assertEquals(expectedNumberOfEntries, Optional.of(taskRepository.getSize(USER_ID_1)));
    }

    @Test
    public void testFindAll() throws Exception {
        Assert.assertEquals(NUMBER_OF_ENTRIES, (Integer) taskRepository.findAll(USER_ID_1).size());
    }

    @Test
    public void testFindAllWithComparator() throws Exception {
        @NotNull final String sortType = "BY_STATUS";
        @Nullable final TaskSort sort = TaskSort.toSort(sortType);
        if (sort != null) {
            Assert.assertEquals(sortType, TaskSort.BY_STATUS.toString());
            Assert.assertEquals(NUMBER_OF_ENTRIES, (Integer) taskRepository.findAll(String.valueOf(CreatedComparator.INSTANCE)).size());
        }
    }

    @Test
    public void testFindAllForUser() throws Exception {
        Assert.assertEquals(NUMBER_OF_ENTRIES - taskRepository.findAll(USER_ID_2).size(),
                taskRepository.findAll(USER_ID_1).size());
    }

    @Test
    public void testFindOneById() throws Exception {
        @NotNull final String wrongId = "501";
        @NotNull Task task = new Task();
        task.setName(TEST_NAME);
        task.setDescription(TEST_DESCRIPTION);
        taskRepository.add(USER_ID_1, task);
        Assert.assertEquals(task, taskRepository.findOneById(USER_ID_1, task.getId()));
        Assert.assertNotEquals(task, taskRepository.findOneById(USER_ID_1, wrongId));
    }

    @Test
    public void testFindOneByIdForUser() throws Exception {
        @NotNull final String wrongId = "501";
        @NotNull Task task = new Task();
        task.setName(TEST_NAME);
        task.setDescription(TEST_DESCRIPTION);
        taskRepository.add(USER_ID_1, task);
        Assert.assertEquals(task, taskRepository.findOneById(USER_ID_1, task.getId()));
        Assert.assertNotEquals(task, taskRepository.findOneById(USER_ID_2, task.getId()));
        Assert.assertNotEquals(task, taskRepository.findOneById(USER_ID_1, wrongId));
        Assert.assertNotEquals(task, taskRepository.findOneById(USER_ID_2, wrongId));
    }

    @Test
    public void testFindOneByIndex() throws Exception {
        @NotNull Task task = new Task();
        task.setName(TEST_NAME);
        task.setDescription(TEST_DESCRIPTION);
        taskRepository.add(USER_ID_1, task);
        Assert.assertEquals(task, taskRepository.findOneByIndex(USER_ID_1, NUMBER_OF_ENTRIES));
        Assert.assertNotEquals(task, taskRepository.findOneByIndex(USER_ID_1, NUMBER_OF_ENTRIES - 1));
    }

    @Test
    public void testFindOneByIndexForUser() throws Exception {
        @NotNull Task task = new Task();
        task.setName(TEST_NAME);
        task.setDescription(TEST_DESCRIPTION);
        taskRepository.add(USER_ID_1, task);
        Assert.assertEquals(task,
                taskRepository.findOneByIndex(USER_ID_1, taskRepository.findAll(USER_ID_1).size() - 1));
        Assert.assertNotEquals(task,
                taskRepository.findOneByIndex(USER_ID_2, taskRepository.findAll(USER_ID_2).size() - 1));
        Assert.assertNotEquals(task,
                taskRepository.findOneByIndex(USER_ID_1, taskRepository.findAll(USER_ID_1).size() - 2));
        Assert.assertNotEquals(task,
                taskRepository.findOneByIndex(USER_ID_2, taskRepository.findAll(USER_ID_2).size() - 2));
    }

    @Test
    public void testGetSize() throws Exception {
        Assert.assertEquals(NUMBER_OF_ENTRIES, Optional.of(taskRepository.getSize(USER_ID_1)));
        Assert.assertEquals(taskList.size(), taskRepository.getSize(USER_ID_1));
    }

    @Test
    public void testGetSizeForUser() throws Exception {
        Assert.assertEquals(NUMBER_OF_ENTRIES / 2 + 1, taskRepository.getSize(USER_ID_1));
        Assert.assertEquals(NUMBER_OF_ENTRIES / 2 - 1, taskRepository.getSize(USER_ID_2));
    }

    @Test
    public void testRemoveOne() throws Exception {
        @NotNull Task task = new Task();
        task.setName(TEST_NAME);
        task.setDescription(TEST_DESCRIPTION);
        taskRepository.add(USER_ID_1, task);
        Assert.assertEquals(task, taskRepository.removeOne(USER_ID_1, task));
        Assert.assertNull(taskRepository.findOneById(USER_ID_1, task.getId()));
    }

    @Test
    public void testRemoveOneForUser() throws Exception {
        @NotNull Task task = new Task();
        task.setName(TEST_NAME);
        task.setDescription(TEST_DESCRIPTION);
        taskRepository.add(USER_ID_1, task);
        Assert.assertEquals(task, taskRepository.removeOne(USER_ID_1, task));
        Assert.assertNull(taskRepository.findOneById(USER_ID_1, task.getId()));
    }

    @Test
    public void testRemoveOneById() throws Exception {
        @NotNull Task task = new Task();
        task.setName(TEST_NAME);
        task.setDescription(TEST_DESCRIPTION);
        taskRepository.add(USER_ID_1, task);
        Assert.assertEquals(task, taskRepository.removeOneById(USER_ID_1, task.getId()));
        Assert.assertNull(taskRepository.findOneById(USER_ID_1, task.getId()));
    }

    @Test
    public void testRemoveOneByIdForUser() throws Exception {
        @NotNull Task task = new Task();
        task.setName(TEST_NAME);
        task.setDescription(TEST_DESCRIPTION);
        taskRepository.add(USER_ID_1, task);
        Assert.assertEquals(task, taskRepository.removeOneById(USER_ID_1, task.getId()));
        Assert.assertNull(taskRepository.findOneById(USER_ID_1, task.getId()));
    }

    @Test
    public void testRemoveOneByIndex() throws Exception {
        @NotNull Task task = new Task();
        task.setName(TEST_NAME);
        task.setDescription(TEST_DESCRIPTION);
        taskRepository.add(USER_ID_1, task);
        Assert.assertEquals(task, taskRepository.removeOneByIndex(USER_ID_1, NUMBER_OF_ENTRIES));
        Assert.assertNull(taskRepository.findOneById(USER_ID_1, task.getId()));
    }

    @Test
    public void testRemoveOneByIndexForUser() throws Exception {
        @NotNull Task task = new Task();
        task.setName(TEST_NAME);
        task.setDescription(TEST_DESCRIPTION);
        taskRepository.add(USER_ID_1, task);
        Assert.assertEquals(task, taskRepository.removeOneByIndex(USER_ID_1, NUMBER_OF_ENTRIES / 2 + 1));
        Assert.assertNull(taskRepository.findOneById(USER_ID_1, task.getId()));
    }

    @Test
    public void testRemoveAllForUser() throws Exception {
        @NotNull final List<Task> emptyList = new ArrayList<>();
        taskRepository.removeAll(USER_ID_1);
        Assert.assertEquals(emptyList, taskRepository.findAll(USER_ID_1));
        Assert.assertNotEquals(emptyList, taskRepository.findAll(USER_ID_2));
    }

    @Test
    public void testRemoveAll() throws Exception {
        final int expectedNumberOfEntries = 0;
        taskRepository.removeAll(USER_ID_1);
        Assert.assertEquals(expectedNumberOfEntries, (long) taskRepository.getSize(USER_ID_1));
    }
}
