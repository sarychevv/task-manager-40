package ru.t1.sarychevv.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.sarychevv.tm.api.service.*;
import ru.t1.sarychevv.tm.enumerated.Role;
import ru.t1.sarychevv.tm.exception.field.*;
import ru.t1.sarychevv.tm.exception.user.ExistsLoginException;
import ru.t1.sarychevv.tm.exception.user.UserNotFoundException;
import ru.t1.sarychevv.tm.marker.DBCategory;
import ru.t1.sarychevv.tm.model.User;

import java.util.ArrayList;
import java.util.List;

@Category(DBCategory.class)
public class UserServiceTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    private static final String TEST_LOGIN = "Test login";

    private static final String TEST_PASSWORD = "Test password";

    private static final String TEST_EMAIL = "Test email";

    private static final String TEST_FIRST_NAME = "Test first name";

    private static final String TEST_LAST_NAME = "Test last name";

    private static final String TEST_MIDDLE_NAME = "Test middle name";
    @NotNull
    private final IPropertyService propertyService = new PropertyService();
    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);
    @NotNull
    private final IProjectService projectService = new ProjectService(connectionService);
    @NotNull
    private final ITaskService taskService = new TaskService(connectionService);
    @NotNull
    private List<User> userList;
    @NotNull
    private IUserService userService;

    @Before
    public void initRepository() throws Exception {
        userList = new ArrayList<>();
        userService = new UserService(propertyService, connectionService, projectService, taskService);

        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final User user = new User();
            user.setLogin("user" + i);
            user.setEmail("user@" + i + ".ru");
            user.setRole(Role.USUAL);
            userService.create("user" + i, String.valueOf(i),"user@" + i + ".ru");
            userList.add(user);
        }
    }

    @Test
    public void testCreate() throws Exception {
        @Nullable final User user = userService.create(TEST_LOGIN, TEST_PASSWORD);
        Assert.assertEquals(user, userService.findOneById(user.getId()));
    }

    @Test
    public void testCreateWithEmail() throws Exception {
        @Nullable final User user = userService.create(TEST_LOGIN, TEST_PASSWORD, TEST_EMAIL);
        Assert.assertEquals(user, userService.findOneById(user.getId()));
    }

    @Test
    public void testCreateWithRole() throws Exception {
        @Nullable final User user = userService.create(TEST_LOGIN, TEST_PASSWORD, Role.ADMIN);
        Assert.assertEquals(user, userService.findOneById(user.getId()));
        Assert.assertEquals(Role.ADMIN, user.getRole());
    }

    @Test(expected = LoginEmptyException.class)
    public void testCreateWithoutLogin() throws Exception {
        userService.create(null, TEST_PASSWORD);
    }

    @Test(expected = ExistsLoginException.class)
    public void testCreateLoginExists() throws Exception {
        userService.create(TEST_LOGIN, TEST_PASSWORD);
        userService.create(TEST_LOGIN, TEST_PASSWORD);
    }

    @Test(expected = PasswordEmptyException.class)
    public void testCreateWithoutPassword() throws Exception {
        userService.create(TEST_LOGIN, null);
    }

    @Test(expected = EmailEmptyException.class)
    public void testCreateWithoutEmail() throws Exception {
        userService.create(TEST_LOGIN, TEST_PASSWORD, (String) null);
    }

    @Test(expected = RoleEmptyException.class)
    public void testCreateWithoutRole() throws Exception {
        userService.create(TEST_LOGIN, TEST_PASSWORD, (Role) null);
    }

    @Test
    public void testFindByLogin() throws Exception {
        Assert.assertEquals(userList.get(1), userService.findByLogin("user2"));
        Assert.assertEquals(userList.get(0), userService.findByLogin("user1"));
    }

    @Test(expected = LoginEmptyException.class)
    public void testFindByLoginWithoutLogin() throws Exception {
        Assert.assertEquals(userList.get(0), userService.findByLogin(null));
    }

    @Test
    public void testRemoveOne() throws Exception {
        Assert.assertEquals(userList.get(1), userService.removeOne(userList.get(1)));
    }

    @Test
    public void testRemoveByLogin() throws Exception {
        Assert.assertEquals(userList.get(1), userService.removeByLogin(userList.get(1).getLogin()));
    }

    @Test(expected = LoginEmptyException.class)
    public void testRemoveByLoginWithoutLogin() throws Exception {
        userService.removeByLogin(null);
    }

    @Test
    public void testSetPassword() throws Exception {
        Assert.assertNotNull(userService.setPassword(userList.get(0).getId(), TEST_PASSWORD));
    }

    @Test(expected = IdEmptyException.class)
    public void testSetPasswordWithoutId() throws Exception {
        userService.setPassword(null, TEST_PASSWORD);
    }

    @Test(expected = PasswordEmptyException.class)
    public void testSetPasswordWithoutPassword() throws Exception {
        userService.setPassword(userList.get(1).getId(), null);
    }

    @Test
    public void testUpdateUser() throws Exception {
        @NotNull final String id = userList.get(0).getId();
        userService.updateUser(id, TEST_FIRST_NAME, TEST_LAST_NAME, TEST_MIDDLE_NAME);
        @Nullable final User user = userService.findOneById(id);
        if (user == null) return;
        Assert.assertEquals(TEST_FIRST_NAME, user.getFirstName());
        Assert.assertEquals(TEST_LAST_NAME, user.getLastName());
        Assert.assertEquals(TEST_MIDDLE_NAME, user.getMiddleName());
    }

    @Test(expected = UserNotFoundException.class)
    public void testUpdateUserWithWrongId() throws Exception {
        userService.updateUser("501", TEST_FIRST_NAME, TEST_LAST_NAME, TEST_MIDDLE_NAME);
    }

    @Test(expected = IdEmptyException.class)
    public void testUpdateUserWithoutId() throws Exception {
        userService.updateUser(null, TEST_FIRST_NAME, TEST_LAST_NAME, TEST_MIDDLE_NAME);
    }

    @Test
    public void testIsLoginExist() throws Exception {
        Assert.assertTrue(userService.isLoginExist("user4"));
    }

    @Test
    public void testIsEmailExist() throws Exception {
        Assert.assertTrue(userService.isEmailExist("user@4.ru"));
    }

    @Test
    public void testLockUserByLogin() throws Exception {
        @NotNull final User user = userService.lockUserByLogin(userList.get(0).getLogin());
        Assert.assertTrue(user.getLocked());
    }

    @Test(expected = LoginEmptyException.class)
    public void testLockUserByLoginWithoutLogin() throws Exception {
        userService.lockUserByLogin(null);
    }

    @Test(expected = UserNotFoundException.class)
    public void testLockUserByIdWithoutId() throws Exception {
        userService.lockUserByLogin("501");
    }

    @Test
    public void testUnlockUserByLogin() throws Exception {
        @NotNull final User user = userService.unlockUserByLogin(userList.get(0).getLogin());
        Assert.assertFalse(user.getLocked());
    }

    @Test(expected = LoginEmptyException.class)
    public void testUnlockUserByLoginWithoutLogin() throws Exception {
        userService.unlockUserByLogin(null);
    }

    @Test
    public void testFindOneById() throws Exception {
        Assert.assertNotNull(userService.findOneById(userList.get(0).getId()));
    }

}
