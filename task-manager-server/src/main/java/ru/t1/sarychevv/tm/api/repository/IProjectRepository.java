package ru.t1.sarychevv.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sarychevv.tm.model.Project;

import java.util.List;

public interface IProjectRepository {

    @Update("UPDATE tm_project SET name = #{name}, created = #{created}, description = #{description}, " +
            "user_id = #{userId}, status = #{status} WHERE id = #{id}")
    void update(@NotNull Project project);

    @NotNull
    @Select("SELECT * FROM tm_project WHERE user_id = #{userId}")
    @Results(value = {@Result(property = "userId", column = "user_id")})
    List<Project> findAll(@NotNull @Param("userId") String userId);

    @NotNull
    @Select("SELECT * FROM tm_project")
    @Results(value = {@Result(property = "userId", column = "user_id")})
    List<Project> findAll();

    @Nullable
    @Select("SELECT * FROM tm_project WHERE user_id = #{userId} ORDER BY name")
    @Results(value = {@Result(property = "userId", column = "user_id")})
    List<Project> findAllOrderByName(@NotNull @Param("userId") String userId);

    @Nullable
    @Select("SELECT * FROM tm_project WHERE user_id = #{userId} ORDER BY created")
    @Results(value = {@Result(property = "userId", column = "user_id")})
    List<Project> findAllOrderByCreated(@NotNull @Param("userId") String userId);

    @Nullable
    @Select("SELECT * FROM tm_project WHERE user_id = #{userId} ORDER BY status")
    @Results(value = {@Result(property = "userId", column = "user_id")})
    List<Project> findAllOrderByStatus(@NotNull @Param("userId") String userId);

    @Nullable
    @Select("SELECT * FROM tm_project WHERE id = #{id} AND user_id = #{userId} LIMIT 1")
    @Results(value = {@Result(property = "userId", column = "user_id")})
    Project findOneById(@NotNull @Param("userId") String userId, @NotNull @Param("id") String id);

    @Nullable
    @Select("SELECT * FROM tm_project WHERE id = #{id} LIMIT 1")
    @Results(value = {@Result(property = "userId", column = "user_id")})
    Project findOneById(@NotNull @Param("id") String id);

    @Nullable
    @Select("SELECT * FROM tm_project WHERE user_id = #{userId} LIMIT 1 OFFSET #{index}")
    @Results(value = {@Result(property = "userId", column = "user_id")})
    Project findOneByIndex(@NotNull @Param("userId") String userId, @NotNull @Param("index") Integer index);

    @Select("SELECT COUNT(*) FROM tm_project WHERE user_id = #{userId}")
    int getSize(@NotNull @Param("userId") String userId);

    @Delete("DELETE FROM tm_project WHERE user_id = #{userId}")
    void removeAll(@NotNull @Param("userId") String userId);

    @Nullable
    @Delete("DELETE FROM tm_project WHERE user_id = #{userId} AND id = #{id}")
    Project removeOne(@NotNull @Param("userId") String userId, @NotNull Project model);

    @Nullable
    @Delete("DELETE FROM tm_project WHERE user_id = #{userId} AND id = #{id}")
    Project removeOneById(@NotNull @Param("userId") String userId,  @NotNull @Param("id") String id);

    @Nullable
    @Delete("DELETE FROM tm_project WHERE user_id = #{userId} AND id = (SELECT id FROM tm_project WHERE user_id = #{userId} LIMIT 1 OFFSET #{index})")
    Project removeOneByIndex(@NotNull @Param("userId") String userId, @NotNull @Param("index") Integer index);

    @Nullable
    @Insert("INSERT INTO tm_project (id, name, created, description, user_id, status)" +
            " VALUES (#{id}, #{name}, #{created}, #{description}, #{userId}, #{status})")
    Project add(@NotNull @Param("userId") String userId, @NotNull Project project);

    @Insert("INSERT INTO tm_project (id, name, created, description, user_id, status)" +
            " VALUES (#{id}, #{name}, #{created}, #{description}, #{userId}, #{status})")
    Project add(@NotNull Project project);

}
