package ru.t1.sarychevv.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sarychevv.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskRepository {

    @Nullable
    @Select("SELECT * FROM tm_task WHERE user_id = #{userId} AND project_id = #{projectId}")
    @Results(value = {@Result(property = "userId", column = "user_id"),
                      @Result(property = "projectId", column = "project_id")})
    List<Task> findAllByProjectId(@NotNull @Param("userId") String userId, @NotNull @Param("projectId") String projectId);

    @Update("UPDATE tm_task SET name = #{name}, created = #{created}, description = #{description}, " +
            "user_id = #{userId}, status = #{status}, project_id = #{projectId} WHERE id = #{id}")
    void update(@NotNull Task task);

    @NotNull
    @Select("SELECT * FROM tm_task WHERE user_id = #{userId}")
    @Results(value = {@Result(property = "userId", column = "user_id"),
                      @Result(property = "projectId", column = "project_id")})
    List<Task> findAll(@NotNull @Param("userId") String userId);

    @NotNull
    @Select("SELECT * FROM tm_task")
    @Results(value = {@Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")})
    List<Task> findAll();

    @Select("SELECT * FROM tm_task WHERE user_id = #{userId} ORDER BY name")
    @Results(value = {@Result(property = "userId", column = "user_id"),
                      @Result(property = "projectId", column = "project_id")})
    @Nullable List<Task> findAllOrderByName(@NotNull @Param("userId") String userId);

    @Select("SELECT * FROM tm_task WHERE user_id = #{userId} ORDER BY created")
    @Results(value = {@Result(property = "userId", column = "user_id"),
                      @Result(property = "projectId", column = "project_id")})
    @Nullable List<Task> findAllOrderByCreated(@NotNull @Param("userId") String userId);

    @Select("SELECT * FROM tm_task WHERE user_id = #{userId} ORDER BY status")
    @Results(value = {@Result(property = "userId", column = "user_id"),
                      @Result(property = "projectId", column = "project_id")})
    @Nullable List<Task> findAllOrderByStatus(@NotNull @Param("userId") String userId);

    @Nullable
    @Select("SELECT * FROM tm_task WHERE id = #{id} AND user_id = #{userId} LIMIT 1")
    @Results(value = {@Result(property = "userId", column = "user_id"),
                      @Result(property = "projectId", column = "project_id")})
    Task findOneById(@NotNull @Param("userId") String userId, @NotNull @Param("id") String id);

    @Nullable
    @Select("SELECT * FROM tm_task WHERE user_id = #{userId} LIMIT 1 OFFSET #{index}")
    @Results(value = {@Result(property = "userId", column = "user_id"),
                      @Result(property = "projectId", column = "project_id")})
    Task findOneByIndex(@NotNull @Param("userId") String userId, @NotNull @Param("index") Integer index);

    @Select("SELECT COUNT(*) FROM tm_task WHERE user_id = #{userId}")
    int getSize(@NotNull @Param("userId") String userId);

    @Delete("DELETE FROM tm_task WHERE user_id = #{userId}")
    void removeAll(@NotNull @Param("userId") String userId);

    @Nullable
    @Delete("DELETE FROM tm_task WHERE user_id = #{userId} AND id = #{id}")
    Task removeOne(@NotNull @Param("userId") String userId, @NotNull Task model);

    @Nullable
    @Delete("DELETE FROM tm_task WHERE user_id = #{userId} AND id = #{id}")
    Task removeOneById(@NotNull @Param("userId") String userId, @NotNull @Param("id") String id);

    @Nullable
    @Delete("DELETE FROM tm_task WHERE user_id = #{userId} AND id = (SELECT id FROM tm_task WHERE user_id = #{userId} LIMIT 1 OFFSET #{index})")
    Task removeOneByIndex(@NotNull @Param("userId") String userId, @NotNull @Param("index") Integer index);

    @Nullable
    @Insert("INSERT INTO tm_task (id, name, created, description, user_id, status, project_id)" +
            " VALUES (#{id}, #{name}, #{created}, #{description}, #{userId}, #{status}, #{projectId})")
    Task add(@NotNull @Param("userId") String userId, @NotNull Task model);

}
